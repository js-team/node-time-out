# time-out

Timeout is a library that wraps setTimeout to provide an easy way to clear timeouts.

```js
import timeout from 'time-out';

const cancel = timeout(() => console.log('I shall not be called.'), 4000);
cancel();
```