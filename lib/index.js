"use strict";

module.exports = function timeout(fn, wait) {
  for (var _len = arguments.length, fnArgs = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    fnArgs[_key - 2] = arguments[_key];
  }

  var timeoutID = setTimeout.apply(undefined, [function () {
    timeoutID = null;
    fn.apply(undefined, arguments);
  }, wait].concat(fnArgs));

  return function cancel() {
    if (timeoutID) {
      clearTimeout(timeoutID);
      timeoutID = null;
    }
  };
};
