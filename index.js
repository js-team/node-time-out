module.exports = function timeout(fn, wait, ...fnArgs) {
  let timeoutID = setTimeout(
    (...args) => {
      timeoutID = null;
      fn(...args);
    },
    wait,
    ...fnArgs
  );

  return function cancel() {
    if (timeoutID) {
      clearTimeout(timeoutID);
      timeoutID = null;
    }
  }
}